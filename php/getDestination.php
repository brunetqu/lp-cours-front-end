
<?php
header('Access-Control-Allow-Origin: *');
// Pour lire votre json
$sourceJson = file_get_contents('destinations.json');
$data = json_decode($sourceJson, true);

echo json_encode($data);

?>