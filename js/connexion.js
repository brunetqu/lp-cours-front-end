
function connexion() {

    // get the login and password
    const login = document.getElementById("login-login").value;
    const password = document.getElementById("login-password").value;

    $.post("http://localhost:8000/php/connexion.php", {login:login, password: password}, function(data, status){
        if (data == "Success") {
            location.reload();
        } else if(data == "Failed") {
            alert("Login ou mot de passe incorrect");
        }else{
            alert("Erreur");
        }
    });
}

function deconnexion() {
    $.post("http://localhost:8000/php/deconnexion.php", function(data, status){
        if (data == "Success") {
            location.reload();
        } else {
            alert("Erreur");
        }
    });
}

function hideConnexion(){
    if(sessionName !== ""){
        document.getElementById("connexion").style.display = "none";
    }
}

function hideDeconnexion(){
    if(sessionName === ""){
        document.getElementById("deconnexion").style.display = "none";
    }
}