
let destination;

class Destination{

    constructor(existingDest, isAdmin, isUser){
        // stockage des destinations
        this.destinations = []

        this.isUser = isUser

        // initialise html table
        // const table = `<table>${this.getHeader()}<tbody></tbody></table>`
        // get the destination div
        // const destinationDiv = document.getElementById("table-destination")
        // destinationDiv.innerHTML = ""
        // destinationDiv.innerHTML = table

        this.setIsAdmin(isAdmin)

        if(existingDest && existingDest.length > 0){
            // parcours des destinations et ajout
            existingDest.forEach((dest) => {
                this.addOrEditDestination(dest)
            })
        }

        this.dialog = new Dialog()
    }

    // if admin set to true
    setIsAdmin(isAdmin){
        this.isAdmin = isAdmin
        if(this.isAdmin){
            // const element = document.querySelector("thead tr")
            // element.innerHTML = `<th>Actions</th>` + element.innerHTML
            // get the main balise element
            const main = document.querySelector("main")
            // add the button to add a destination
            main.innerHTML += `<button onclick="destination.openDestinationDialog()" class="add-destination"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.2.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M256 80c0-17.7-14.3-32-32-32s-32 14.3-32 32V224H48c-17.7 0-32 14.3-32 32s14.3 32 32 32H192V432c0 17.7 14.3 32 32 32s32-14.3 32-32V288H400c17.7 0 32-14.3 32-32s-14.3-32-32-32H256V80z"/></svg></button>`
        }
    }

    // construct proper object
    addDestination(id, destination, image, offre, prix, description){
        const newDest = {
            id: id,
            destination: destination,
            image: image,
            offre: offre,
            prix: prix,
            description: description
        }
        this.addOrEditDestination(newDest)
    }

    // remove one destination by it's id
    removeDestination(id){
        this.destinations = this.destinations.filter((dest) => dest.id !== id)
        // remove the destination with corresponding id
        const div = document.getElementById(`destination-id-${id}`)
        div.remove()
        saveJsonData(this.destinations)
    }

    // get one destination by it's id
    getDestination(id){
        const getdest = this.destinations.find((dest) => dest.id == id)
        return getdest
    }

    // add the destination if the id is new, modify if the id already exist
    addOrEditDestination(newdest){
        const i = this.destinations.findIndex((dest) => dest.id === newdest.id)
        // if id already exist
        if(i !== -1){
            const element = document.getElementById(`destination-id-${newdest.id}`)
            element.innerHTML = this.getOneDestination(newdest) 
            this.destinations[i] = newdest
        }else {
            this.destinations.push(newdest)
            // const element = document.querySelector("tbody")
            const element = document.getElementById("table-destination")
            //element.innerHTML += this.getOneDestination(newdest)
            element.innerHTML += this.getHtmlDestination(newdest)
        }
    }

    // Open the dialog to modify or create a destination
    // if create id is null
    openDestinationDialog(id){
        this.dialog.close()
        let destinationObject;
        if(!id) {
            destinationObject = {
                // create "uniq" id
                id: Date.now(),
                destination: "",
                image: "",
                offre: "",
                prix: "",
                description: ""
            }
        } else {
            // if we modify then get destination object
            destinationObject = this.getDestination(id)
        }

        // display the dialog
        this.dialog.open()
        const destinationBlur = document.getElementById("destination-blur")
        destinationBlur.addEventListener("click", (e) => {
            this.closeDestinationDialog(destinationObject)
        })

        // for all key in the object, add event listener when change to the proper input
        Object.keys(destinationObject).forEach((key) => {
            const element = document.getElementById("dialog-" + key)

            // if element exist, for exemple id don't have input
            if(element) {
                element.value = destinationObject[key]
                element.addEventListener("change", (e) => {
                    destinationObject[key] = e.target.value
                })
            }
        })

        const image_input = document.getElementById("dialog-image64");

        document.getElementById("display-image").style.backgroundImage = `url(${destinationObject.image})`

        image_input.addEventListener("change", function() {
            const reader = new FileReader();
            reader.addEventListener("load", () => {
                const uploaded_image = reader.result;
                document.getElementById("display-image").style.backgroundImage = `url(${uploaded_image})`;
                destinationObject['image'] = uploaded_image
            });
            reader.readAsDataURL(this.files[0]);
        });

        document.getElementById("dialog-save").addEventListener("click",(e) => {
            e.preventDefault()
            this.closeDestinationDialog(destinationObject)
            this.addOrEditDestination(destinationObject)
            saveJsonData(this.destinations)
        })
    }

    closeDestinationDialog(destinationObject){
        // close dialog
        this.dialog.close()
    }

    // get One html element for a destination
    getOneDestination(destination) {

        let adminButtons;
        let userButtons;
        if(this.isAdmin){
            adminButtons = `
                <td>
                    <i class="fa-solid fa-xmark"></i>
                    <button onclick="destination.removeDestination('${destination.id}')">Supprimer</button>
                    <button onclick="destination.openDestinationDialog('${destination.id}')">Modifier</button>
                </td>
            `
        }

        if(this.isUser){
            userButtons = `<button onclick="">Découvrir</button>`
        }

        return `<tr id="destination-id-${destination.id}">       
                ${adminButtons ? adminButtons : ""}         
                <td>${destination.destination}</td>
                <td><img src="${destination.image}"></td>
                <td>${destination.offre}</td>
                <td>${destination.prix}</td>
                <td>${destination.description}${userButtons ? userButtons : ""}</td>
        </tr>`
    }

    // get the html element of the header
    getHeader() {
        return `<thead>
            <th>Destination</th>
            <th>Image</th>
            <th>Offre</th>
            <th>Prix</th>
            <th>Description</th>
        </thead>`
    }

    // get html of one destination (new Tp04)
    getHtmlDestination(destination) {

        let adminButtons;
        let userButtons;
        if(this.isAdmin){
            adminButtons = `
                <div class="admin-buttons">
                    <button onclick="destination.removeDestination('${destination.id}')">Supprimer</button>
                    <button onclick="destination.openDestinationDialog('${destination.id}')">Modifier</button>
                </div>
            `
        }

        if(this.isUser){
            userButtons = `<button class="discover" onclick="">Découvrir</button>`
        }

        return `<div class="destination-widget" id=${`destination-id-${destination.id}`}>
            ${userButtons ? userButtons : ""}
            <div class="image-container">
                <div class="destination-image" style="background-image: url('${destination.image}')"></div>
            </div>
            <div class="destination-info">
                <div class="destination-content">
                    <h2>${destination.destination}</h2>
                    <p>${destination.description}</p>
                </div>
                <div>
                    <h4>${destination.prix}</h2>
                    <p>${destination.offre}</p>
                </div>
            </div>
            ${adminButtons ? adminButtons : ""}
        </div>`
    }
        
}

class Dialog {

    constructor(){
        this.dialog = `<div id="dialog">
            <div id="destination-blur"></div>
            <div id="destination-dialog">
                <form>
                    <div class="form-group">
                        <label for="destination">Destination</label>
                        <input type="text" id="dialog-destination" />
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" id="dialog-image64" accept="image/jpeg, image/png, image/jpg" />
                        <div id="display-image"></div>
                    </div>
                    <div class="form-group">
                        <label for="offre">Offre</label>
                        <input type="text" id="dialog-offre" />
                    </div>
                    <div class="form-group">
                        <label for="prix">Prix</label>
                        <input type="text" id="dialog-prix" />
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea id="dialog-description" rows="4"></textarea>
                    </div>
                    <button onClick="" id="dialog-save">Sauvegarder</button>
                </form>
            </div>
        </div>`
    }

    open(){
        // add the dialog to the body
        document.body.innerHTML += this.dialog

    }

    close(){
        // remove the dialog
        const dialog = document.getElementById("dialog")
        // if dialog is open
        if(dialog){
            dialog.remove()
        }
    }
}

function displayDestinations(){

    getJsonData("destinations.json").then((data) => {
        destination = new Destination(data, sessionName === "admin", sessionName !== "" ? true : false)
    })
}