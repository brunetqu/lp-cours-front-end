

// chose a number between 0 and 10 and make it guess to the user

// 1. chose a number between 0 and 10
function choseNumber() {
  return Math.floor(Math.random() * 10);
}

// 2. make it guess to the user with a counter
function guessNumberWithCounter() {
    // store the number of tries
    let tries = 0;
    let number = choseNumber();
    let guess = prompt("Guess a number between 0 and 10");
    while (guess != number) {
        if(guess > number) {
            guess = prompt("Too high, try again");
        } else if (guess < number) {
            guess = prompt("Too low, try again");
        }
        tries++;
    }
    alert("You win in " + (tries + 1) + " tries");
}

guessNumberWithCounter()