

// object with european states and their corresponding cities

const states = {
    "France": ["Paris", "Lyon", "Marseille", "Toulouse"],
    "Belgique": ["Bruxelles", "Liège", "Namur", "Anvers"],
    "Allemagne": ["Berlin", "Hambourg", "Munich",  "Cologne"],
    "Italie": ["Rome", "Milan", "Venise", "Turin"],
    "Espagne": ["Madrid", "Barcelone", "Valence", "Séville"],
    "Portugal": ["Lisbonne", "Porto", "Faro", "Braga"],
    "Royaume-Uni": ["Londres", "Birmingham", "Glasgow", "Liverpool"],
    "Pays-Bas": ["Amsterdam", "Rotterdam", "La Haye", "Utrecht"],
    "Suisse": ["Berne", "Genève", "Zurich", "Lausanne"],
}

// function to create a table with single state and its corresponding cities

function createTable(state) {
    
        // create the table
    
        const table = document.createElement("table");
    
        // create the header
    
        const header = document.createElement("thead");
        const headerRow = document.createElement("tr");
        const headerCell = document.createElement("th");
        headerCell.textContent = state;
        headerRow.appendChild(headerCell);
        header.appendChild(headerRow);
        table.appendChild(header);
    
        // create the body
    
        const body = document.createElement("tbody");
        for (let city of states[state]) {
            const bodyRow = document.createElement("tr");
            const bodyCell = document.createElement("td");
            bodyCell.textContent = city;
            bodyRow.appendChild(bodyCell);
            body.appendChild(bodyRow);
        }
        table.appendChild(body);
    
        return table;
}



// function to happend table to the body

function appendTables(table) {
    // get element by id
    const tableContainer = document.getElementById("table-container");
    tableContainer.appendChild(table);
}

// function that is call when input is changed and indicate to the user the possibilities of cities

function searchCity(e) {
    // get the value of the input
    const input = e.target.value;

    // get all td elements
    const cities = document.querySelectorAll("td");

    // loop through the td elements
    for (let city of cities) {
        // if the start of input is the same as the start of the city then surlign the city
        if (input!=="" && city.textContent.startsWith(input)) {
            city.style.backgroundColor = "yellow";
        }
        // else remove the surlign
        else {
            city.style.backgroundColor = "";
        }
    }
}

function manageButton(e) {
    const paySearchButton = document.getElementById("pays-search-button");

    // get input value
    const input = e.target.value;
    // if the input match with one cities of states then enable the button*
    if (input !== "" && Object.values(states).flat().includes(input)) {
        paySearchButton.disabled = false;
    }// else disable the button
    else {
        paySearchButton.disabled = true;
    }
}

function searchState(e) {
    const paysSearch = document.getElementById("pays-search");
    const input = paysSearch.value;
    // make dialog with the corresponding state
    alert(`La ville ${input} se trouve en ${Object.keys(states).find(key => states[key].includes(input))} \n vous pouvez également visiter les lieux suivants: ${states[Object.keys(states).find(key => states[key].includes(input))].filter(city => city !== input)}`);
}

// partie 2

function suggestionOnInput() {
    $(function() {
        $( "#pays-search" ).autocomplete({
          source: Object.values(states).flat()
        });
      } 
    );
}

onload = () => {

    // loop through the object and create a table for each state
    for (let state in states) {
        const table = createTable(state);
        appendTables(table);
    }

    const paysSearch = document.getElementById("pays-search");
    paysSearch.addEventListener("keyup", searchCity);

    paysSearch.addEventListener("keyup", manageButton);

    const paySearchButton = document.getElementById("pays-search-button");
    paySearchButton.addEventListener("click", searchState);

    // partie 2
    suggestionOnInput()
}