

let value = 50;
// invoke when input changes
function handleChange(){

    value = parseInt($('#input-percent').val());

    // if the value not set, set it to 50

    if(value !== 0 && !value){
        value = 50;
    }
    // if value is greater than 100, set it to 100
    else if(value > 100){
        value = 100;
    }
    // if value is less than 0, set it to 0
    else if(value < 0){
        value= 0;
    }

    $('#input-percent').val(value);

    $('.loading span').animate({
        width : value + '%'
    },1000);

    $('.loading span').animate({
        backgroundColor: `rgb(${value * 255/100 }, ${value < 33 ? 0 : (value-33) * 255/67 }, ${value < 66 ? 0 : (value-66) * 255/34 })`
    },1000);

}