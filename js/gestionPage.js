

let main = `
<h2>Accueil</h2>
<section>
<h2>Description générale</h2>
    <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt sapiente architecto odio vitae reiciendis nisi perspiciatis. Tempora minus
        impedit illo iste assumenda recusandae repellendus consequuntur perferendis modi, reprehenderit commodi dolor numquam sint voluptate consequatur
        ipsa eum voluptatum doloribus velit veniam laboriosam hic nisi ad? Enim explicabo maiores illo possimus omnis?
    </p>
    <hr />
</section>

<section>
    <h2>Infos pratiques</h2>
    <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt sapiente architecto odio vitae reiciendis nisi perspiciatis. Tempora minus
        impedit illo iste assumenda recusandae repellendus consequuntur perferendis modi, reprehenderit commodi dolor numquam sint voluptate consequatur
        ipsa eum voluptatum doloribus velit veniam laboriosam hic nisi ad? Enim explicabo maiores illo possimus omnis?
    </p>
</section>
`

let perso = `
<h2>Espace perso</h2>
<section class="espace-perso">
<ul>
  <li><a>Connexion</a></li>
  <li>
    <a>Mes informations</a>
    <ol>
      <li>Quentin Brunet</li>
      <li>07 55 69 86 65</li>
      <li>quentin.brunet@gmail.com</li>
    </ol>
  </li>
  <li><a>Messagerie</a></li>
  <li><a>Historique</a></li>
</ul>
</section>
<form id="connexion" onsubmit="connexion()">
    <h2>Se connecter</h2>
    <input type="text" id="login-login" placeholder="Login" />
    <input type="text" id="login-password" placeholder="Mot de passe" />
    <button>Se connecter</button>
</form>
<button onclick="deconnexion()" id="deconnexion">Se déconnecter</button>
`

let destinations = `
    <section>
        <h2>Destinations</h2>
        <div id="table-destination"></div>
    </section>
`

let voyage_audio = `
    <h2>Voyage virtuel audio</h2>
    <section class="voyage-virtuel-audio">
        <figure>
            <figcaption>Lorem ipsum dolor sit amet.</figcaption>
            <audio controls autoplay src="../audio/707.mp3"></audio>
        </figure>
        <figure>
            <figcaption>Lorem ipsum dolor sit amet.</figcaption>
            <audio controls src="../audio/Airplane+4.mp3"></audio>
        </figure>
        <figure>
            <figcaption>Lorem ipsum dolor sit amet.</figcaption>
            <audio controls src="../audio/airplane+b17.mp3"></audio>
        </figure>
        <figure>
            <figcaption>Lorem ipsum dolor sit amet.</figcaption>
            <audio controls src="../audio/airplane+b25-1.mp3"></audio>
        </figure>
    </section>
`

let voyage_video = `
    <h2>Voyage virtuel vidéo</h2>
    <section class="voyage-virtuel-video">
        <div>
            <h3>Cascade</h3>
            <video controls autoplay width="250">
                <source src="../video/file.mp4" type="video/mp4" />
            </video>
            <p>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Unde,
                aliquid.
            </p>
        </div>

        <div>
            <h3>Nuages</h3>
            <video controls width="250">
                <source src="../video/0f71f09a-a2344e76.mp4" type="video/mp4" />
            </video>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing.</p>
        </div>
    </section>
`

let contact = `
    <h2>Contact</h2>
    <form class="contact-form" action="/action_page.php">
        <fieldset>
            <legend>Information perso</legend>
            <label for="nom">Nom:</label>
            <input type="text" required id="nom" name="nom" />

            <label for="prenom">Prenom:</label>
            <input type="text" required id="prenom" name="prenom" />

            <label for="email">Email:</label>
            <input type="email" required id="email" name="email" />

            <label for="tel">Numéro de téléphone:</label>
            <input type="tel" pattern="[0-9]{10}" required id="tel" name="tel" />
        </fieldset>

        <fieldset>
            <legend>Envoyer</legend>

            <label for="message">Message:</label>
            <textarea id="message" name="message" cols="40" rows="5"></textarea>

            <input type="submit" value="Envoyer" />
        </fieldset>
    </form>
`

function display(page){
    let pageHtml;
    switch(page){
        case "main":
            document.querySelector("main").innerHTML = main
            break;
        case "perso":
            document.querySelector("main").innerHTML = perso
            hideConnexion()
            hideDeconnexion()
            break;
        case "destinations":
            document.querySelector("main").innerHTML = destinations
            displayDestinations()
            break;
        case "voyage_audio":
            document.querySelector("main").innerHTML = voyage_audio
            break;
        case "voyage_video":
            document.querySelector("main").innerHTML = voyage_video
            break;
        case "contact":
            document.querySelector("main").innerHTML = contact
            break;
    }
}